<?php

/**
 * @file
 * FeedsFileFetcher class.
 */

/**
 * Class feeds_callback_fetcher.
 */
// @codingStandardsIgnoreStart
class feeds_callback_fetcher extends FeedsFileFetcher {
// @codingStandardsIgnoreEnd

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'callback' => '',
      'source' => NULL,
    );
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array();

    $form['callback'] = array(
      '#type' => 'textfield',
      '#title' => t('Callback'),
      '#description' => t('Specify a callback'),
      '#default_value' => $this->config['callback'],
      '#required' => TRUE,
    );

    $form['source'] = array(
      '#type' => 'value',
      '#value' => NULL,
    );

    return $form;
  }

  /**
   * Override parent::configFormValidate().
   */
  public function configFormValidate(&$values) {
    $this->checkCallbackValidation($values);
  }

  /**
   * Fetch content from a source and return it.
   *
   * @param \FeedsSource $source
   *   The source.
   *
   * @return \FeedsFileFetcherResult
   *   The result.
   *
   * @throws \Exception
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);

    if (!is_callable($source_config['callback'])) {
      throw new Exception(t("Callback @callback doesn't exists.", array('@callback' => $source_config['callback'])));
    }

    $file = drupal_tempnam(file_directory_temp(), 'feeds-');
    file_put_contents($file, call_user_func($source_config['callback']));

    if (filesize($file) === 0) {
      throw new Exception(t('Callback returned nothing, temporary file size is empty.'));
    }

    return new FeedsFileFetcherResult($file);
  }

  /**
   * Override parent::sourceDefaults().
   */
  public function sourceDefaults() {
    return array(
      'callback' => '',
      'source' => NULL,
    );
  }

  /**
   * Override parent::sourceForm().
   */
  public function sourceForm($form_state) {
    $form = array();

    $form['callback'] = array(
      '#type' => 'textfield',
      '#title' => t('Callback'),
      '#description' => t('Specify a callback'),
      '#default_value' => $this->config['callback'],
      '#required' => TRUE,
    );

    $form['source'] = array(
      '#type' => 'value',
      '#value' => NULL,
    );

    return $form;
  }

  /**
   * Override parent::configFormValidate().
   */
  public function sourceFormValidate(&$values) {
    $this->checkCallbackValidation($values);
  }

  /**
   * Custom callback to validate the configuration.
   *
   * @param array $values
   *   The values.
   */
  private function checkCallbackValidation(array &$values) {
    $values['callback'] = trim($values['callback']);

    if (!is_callable($values['callback'])) {
      form_set_error('feeds][feeds_callback_fetcher][callback', t('Unable to save, the callback does not exist.'));
    }
  }

}
